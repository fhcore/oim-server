import service from '@/libs/service';

let personal = {};

personal.login = function (account, password, back) {
    var body = {
        'account': account,
        'password': password
    };

    var m = {'body': body};
    service.post('/manage/index/login', m, back);
}

personal.menuList = function (id, back) {
    var body = {
        'id': id
    };
    var m = {'body': body};
    service.post('/manage/index/menuList', m, back);
}

personal.info = function (id, back) {
    var body = {
        'id': id
    };
    var m = {'body': body};
    service.post('/manage/personal/info', m, back);
}

personal.updatePassword = function (id, newPassword, oldPassword, back) {
    var body = {
        'id': id,
        'newPassword': newPassword,
        'oldPassword': oldPassword
    };
    var m = {'body': body};
    service.post('/manage/personal/updatePassword', m, back);
}

export default personal;
