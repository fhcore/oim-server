import * as base from './base';
let dictionary = {};

const map = new base.Map();

function put(category, property, value, name) {
    var propertyMap = getMap(category, property);
    propertyMap.put(value, name);
}

function getMap(category, property) {

    var categoryMap = map.get(category);
    if (!categoryMap) {
        categoryMap = new base.Map();
        map.put(category, categoryMap);
    }

    var propertyMap = categoryMap.get(property);
    if (!propertyMap) {
        propertyMap = new base.Map();

        categoryMap.put(property, propertyMap);
    }
    return propertyMap;
}


dictionary.getName = function (category, property, value) {
    var categoryMap = map.get(category);
    var propertyMap = (categoryMap) ? categoryMap.get(property) : null;
    var name = (propertyMap) ? propertyMap.get(value) : '';
    return name;
};

dictionary.setName = function (list, category, valueProperty, nameProperty, children) {
    if (list) {
        var categoryMap = map.get(category);
        var propertyMap = (categoryMap) ? categoryMap.get(valueProperty) : null;

        var length = list.length;
        for (var i = 0; i < length; i++) {
            var item = list[i];
            var value = item[valueProperty];
            var name = (propertyMap) ? propertyMap.get(value) : '';
            item[nameProperty] = name;
            if (children && '' != children) {
                var nodes = item[children];
                if (nodes) {
                    dictionary.setName(nodes, category, valueProperty, nameProperty, children);
                }
            }
        }
    }
};

initMap();

function initMap () {
    put('article', 'status', '0', '草稿');
    put('article', 'status', '1', '已发表');
    put('article', 'status', '2', '回收站');

    put('articleCategory', 'type', '0', '未分类');
    put('articleCategory', 'type', '1', '广告');
    put('articleCategory', 'type', '2', '公告');
    put('articleCategory', 'type', '3', '资讯');

    put('common', 'flag', '0', '禁用');
    put('common', 'flag', '1', '启用');

    put('area', 'province', '110000', '北京市');
    put('area', 'province', '120000', '天津市');
    put('area', 'province', '130000', '河北省');
    put('area', 'province', '140000', '山西省');
    put('area', 'province', '150000', '内蒙古自治区');
    put('area', 'province', '210000', '辽宁省');
    put('area', 'province', '220000', '吉林省');
    put('area', 'province', '230000', '黑龙江省');
    put('area', 'province', '310000', '上海市');
    put('area', 'province', '320000', '江苏省');
    put('area', 'province', '330000', '浙江省');
    put('area', 'province', '340000', '安徽省');
    put('area', 'province', '350000', '福建省');
    put('area', 'province', '360000', '江西省');
    put('area', 'province', '370000', '山东省');
    put('area', 'province', '410000', '河南省');
    put('area', 'province', '420000', '湖北省');
    put('area', 'province', '430000', '湖南省');
    put('area', 'province', '440000', '广东省');
    put('area', 'province', '450000', '广西壮族自治区');
    put('area', 'province', '460000', '海南省');
    put('area', 'province', '500000', '重庆市');
    put('area', 'province', '510000', '四川省');
    put('area', 'province', '520000', '贵州省');
    put('area', 'province', '530000', '云南省');
    put('area', 'province', '540000', '西藏自治区');
    put('area', 'province', '610000', '陕西省');
    put('area', 'province', '620000', '甘肃省');
    put('area', 'province', '630000', '青海省');
    put('area', 'province', '640000', '宁夏回族自治区');
    put('area', 'province', '650000', '新疆维吾尔自治区');
    put('area', 'province', '710000', '台湾省');
    put('area', 'province', '810000', '香港特别行政区');
    put('area', 'province', '820000', '澳门特别行政区');

}

export default dictionary;
