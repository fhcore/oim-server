/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.im.server.general.common.bean.system;

import java.util.Date;

import javax.persistence.Entity;

import com.im.base.bean.BaseBean;

/**
 * 
 * @author xiahui
 */
@Entity(name = "m_role_menu")
public class RoleMenu extends BaseBean {

	private String roleId;// 角色Id
	private String menuId;
	private Date createTime;// 输入时间


	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
