package com.im.server.general.common.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.im.base.dao.BaseDAO;
import com.im.server.general.common.bean.system.RoleMenu;
import com.onlyxiahui.query.hibernate.QueryWrapper;

/*
 * 
 */
@Repository
public class RoleMenuDAO extends BaseDAO {

	String namespace ="roleMenu";// RoleMenu.class.getName();


	public void delete(String id) {
		writeDAO.deleteById(RoleMenu.class, id);
	}

	public List<RoleMenu> getRoleMenuListByRoleId(String roleId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.addParameter("roleId", roleId);
		List<RoleMenu> list = readDAO.queryListByName(namespace + ".getRoleMenuListByRoleId", queryWrapper, RoleMenu.class);
		return list;
	}

	public void deleteByRoleId(String roleId) {
		StringBuilder sql = new StringBuilder();
		sql.append("delete from m_role_menu where roleId='");
		sql.append(roleId);
		sql.append("'");
		writeDAO.executeSQL(sql.toString());
	}

	public void deleteByMenuId(String menuId) {
		StringBuilder sql = new StringBuilder();
		sql.append("delete from m_role_menu where menuId='");
		sql.append(menuId);
		sql.append("'");
		writeDAO.executeSQL(sql.toString());
	}
}
