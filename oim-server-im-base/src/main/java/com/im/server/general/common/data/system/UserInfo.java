package com.im.server.general.common.data.system;

import java.util.List;

import com.im.server.general.common.bean.User;


/**
 * @author: XiaHui
 */
public class UserInfo extends User {

	private List<UserRoleInfo> userRoleInfoList;

	public List<UserRoleInfo> getUserRoleInfoList() {
		return userRoleInfoList;
	}

	public void setUserRoleInfoList(List<UserRoleInfo> userRoleInfoList) {
		this.userRoleInfoList = userRoleInfoList;
	}

}
